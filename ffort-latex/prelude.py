from pygments.lexer import RegexLexer,bygroups
from pygments.token import *

def merge(*x):
  result = {}
  for x in x:
    result.update(x)
  return result

fullid = r'[\'a-zA-Z][a-zA-Z0-9_\']*'
textid = r'[\'a-zA-Z]+'
rint = r'[0-9]+'

utils = {
  'scol': [
    (r'\s*;\s*', Text, '#pop'),
  ],

  'comma': [
    (r'\s*,\s*', Text, '#pop')
  ],

  'lpar': [
    (r'\s+', Text),
    (r'\(', Text, '#pop')
  ],

  'rpar': [
    (r'\s+', Text),
    (r'\)', Text, '#pop')
  ],

  'local-list': [
    (r'\s+', Text),
    (r',', Text),
    (r'\.\.\.',Text),
    (fullid, Name.Attribute),
    (r';', Text, '#pop'),
  ],
}

vardecl = merge(utils, {
  'vardecl': [
    (r'\s+', Text),
    (fullid, Name.Attribute, ('#pop', 'vardecl-postname')),
  ],

  'vardecl-postname': [
    (r'\s+', Text),
    (r',', Text, ('#pop','vardecl')),
    (r':', Text, ('#pop', 'vardecl-opt-type-opt-clock')),
    (r';', Text, ('#pop', 'vardecl')),
    (r'\)', Text, ('#pop')),
  ],

  'vardecl-opt-type-opt-clock': [
    (r'\s+', Text),
    (r'rate', Keyword, ('#pop', 'vardecl-mid-clock')),
    (textid, Keyword.Type, ('#pop', 'vardecl-type-opt-array')),
  ],

  'vardecl-type-opt-array': [
    (r'\s+', Text),
    (r'\[[0-9]+\]', Keyword.Type),
    (r'', Text, ('#pop', 'vardecl-opt-clock')),
  ],

  'vardecl-opt-clock': [
    (r'\s+', Text),
    (r'rate', Keyword, ('#pop', 'vardecl-mid-clock')),
    (r';', Text, ('#pop', 'vardecl')),
    (r'\)', Text, ('#pop')),
  ],

  'vardecl-mid-clock': [
    (r'\s+', Text),
    (r'\([a-zA-Z]+,[a-zA-Z]+\)', Number, ('#pop', 'vardecl-post-clock', 'vardecl-trailing-on')),
    (r'\(\s*[0-9]+,\s*[0-9]+\s*\)', Number, ('#pop', 'vardecl-post-clock', 'vardecl-trailing-on')),
    (r'ckdecl', Generic.Emph, ('#pop', 'vardecl-post-clock', 'vardecl-trailing-on')),
  ],

  'vardecl-trailing-on': [
    (r'\s+', Text),
    (r'on', Keyword, ('vardecl-on-constructor')),
    (r'', Text, ('#pop')),
  ],

  'vardecl-on-constructor': [
    (r'\s+', Text),
    (fullid, Name.Attribute, ('#pop', 'rpar', 'vardecl-on-condition', 'lpar')),
  ],

  'vardecl-on-condition': [
    (r'\s+', Text),
    (fullid, Name.Variable.Instance, ('#pop')),
  ],

  'vardecl-post-clock': [
    (r'\s+', Text),
    (r';', Text, ('#pop', 'vardecl')),
    (r'\)', Text, ('#pop')),
  ],
})

imported_node = merge(utils, {
  'imported-node': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'node', Text, ('#pop', 'scol', 'ind-props' , 'ind-args', 'ind-returns', 'ind-args', 'ind-name'))
  ],

  'ind-name': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (fullid, Text, '#pop'),
  ],

  'ind-args': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'', Text, ('#pop', 'rpar', 'ind-arg', 'lpar')),
  ],

  'ind-arg': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (fullid, Name.Attribute, ('#pop', 'ind-arg-post-name')),
  ],

  'ind-arg-post-name': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r',', Text, ('#pop', 'ind-arg')), # Read a comma, means we must parse another argument
    (r':', Text, ('#pop', 'ind-arg-type')), # Read a colon, must parse a type
  ],

  'ind-arg-type': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (textid, Keyword.Type, ('#pop', 'ind-arg-opt-array')),
  ],

  'ind-arg-opt-array': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'\[[0-9]+\]', Keyword.Type), # Array dimension, potentially continue parsing
    (r'', Text, ('#pop', 'ind-arg-post-arg')), # no more array dimensions to read
  ],

  'ind-arg-post-arg': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r';', Text, ('#pop', 'ind-arg')), # Read a semicolon, must parse next argument
    (r'', Text, ('#pop')), # None of the above, stop parsing arguments
  ],

  'ind-returns': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'returns', Keyword, ('#pop')),
  ],

  'ind-props': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'', Text, ('#pop', 'ind-props-opt-wcet-opt-stack')),
  ],

  'ind-props-opt-wcet-opt-stack': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'wcet', Keyword, ('#pop', 'ind-props-opt-stack', 'ind-props-wcet')),
    (r'stack', Keyword, ('#pop', 'ind-props-opt-wcet', 'ind-props-stack')),
    (r'', Text, '#pop'),
  ],

  'ind-props-wcet': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (rint, Number, '#pop'),
  ],

  'ind-props-stack': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (rint, Number, '#pop'),
  ],

  'ind-props-opt-wcet': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'wcet', Keyword, ('#pop', 'ind-props-wcet')),
    (r'', Text, '#pop'),
  ],

  'ind-props-opt-stack': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'stack', Keyword, ('#pop', 'ind-props-stack')),
    (r'', Text, '#pop'),
  ],
})

sensact = merge(utils, {
  'sensor': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'sensor', Keyword, ('#pop', 'sensact-mid')),
  ],

  'actuator': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'actuator', Keyword, ('#pop', 'sensact-mid')),
  ],

  'sensact-mid': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (fullid, Text, ('#pop', 'scol', 'sensact-wcet-opt-stack')),
  ],

  'sensact-wcet-opt-stack': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'(wcet)(\s+)([0-9]+)', bygroups(Keyword,Text,Number), ('#pop', 'sensact-opt-stack')),
    (r'(stack)(\s+)([0-9]+)', bygroups(Keyword,Text,Number), ('#pop', 'sensact-wcet')),
  ],

  'sensact-wcet': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'(wcet)(\s+)([0-9]+)', bygroups(Keyword,Text,Number), '#pop'),
  ],


  'sensact-opt-stack': [
    (r'\s+', Text),
    (r'--.*', Comment.Singleline),
    (r'(stack)(\s+)([0-9]+)', bygroups(Keyword,Text,Number), ('#pop')),
    (r'', Text, '#pop'),
  ],
})

expr = merge(utils, {
  'expr': [
    (r'\s*\.\.\.', Text, ('#pop')),
    ('', Text, ('#pop', 'expr_l3')),
  ],

  'expr_l3': [
    (r'\s+', Text),
    (r'if', Keyword, ('#pop', 'expr_l2', 'else', 'expr_l2', 'then', 'expr_l2')),
    (r'', Text, ('#pop', 'expr_l2')),
  ],

  # /!\ This parses in the opposed sense of the actual prelude parser
  'expr_l2': [
    (r'\s+', Text),
    (r'([0-9]+)(\s*)(fby\*?)', bygroups(Number,Text,Keyword), ('#pop', 'expr_l2')),
    (r'('+fullid+r')(\s+)(fby\*?)', bygroups(Name.Constant,Text,Keyword), ('#pop', 'expr_l2')),
    (r'([0-9]+)(\s*)(::)', bygroups(Number,Text,Keyword), ('#pop', 'expr_l2')),
    (r'('+fullid+r')(\s*)(::)', bygroups(Name.Constant,Text,Keyword), ('#pop', 'expr_l2')),
    (r'(tail)(\s+)', bygroups(Keyword, Text), ('#pop', 'expr_l2')),
    (r'', Text, ('#pop', 'expr_l1')),
  ],

  'expr_l1': [
    (r'\s+', Text),
    (r'(merge)(\s*)(\()', bygroups(Keyword,Text,Text), ('#pop', 'mergepairs', 'mergevar')),
    (r'('+fullid+r')(\s*)(\()', bygroups(Name.Function,Text,Text), ('#pop', 'expr_opt_ROp', 'exprapply', 'expr')),
    (r'\(', Punctuation, ('#pop', 'expr_opt_ROp', 'rpar', 'expr')),
    (fullid, Text, ('#pop', 'expr_opt_ROp')),
    (r'[0-9]+', Number, ('#pop', 'expr_opt_ROp')),
  ],

  'expr_ROp': [
    (r'\s+', Text),
    (r'(when)(\s+)('+fullid+r')(\s*\('+fullid+r'\))', bygroups(Keyword,Text,Name.Attribute,Text)),
    (r'(/\^)(\s*)([0-9]+|k)', bygroups(Keyword,Text,Number)),
    (r'(\*\^)(\s*)([0-9]+|k)', bygroups(Keyword,Text,Number)),
    (r'(\~>)(\s*)([0-9]+|k)', bygroups(Keyword,Text,Number)),
    (r'(rate)(\s*)(\([0-9]+,[0-9]+\))(\s*)(on)(\s*)('+fullid+')(\s*)(\()('+fullid+')(\))', bygroups(Keyword,Text,Number,Text,Keyword,Text,Name.Attribute,Text,Text,Name.Variable.Instance,Text)),
    (r'(rate)(\s*)(\([0-9]+,[0-9]+\))', bygroups(Keyword,Text,Number)),
    (r'(rate)(\s*)(\(n,p\))', bygroups(Keyword,Text,Number)),
  ],

  'expr_opt_ROp': [
    (r'\s+', Text),
    (r'(when)(\s+)('+fullid+r')(\s*\('+fullid+r'\))', bygroups(Keyword,Text,Name.Attribute,Text)),
    (r'(/\^)(\s*)([0-9]+|k)', bygroups(Keyword,Text,Number)),
    (r'(\*\^)(\s*)([0-9]+|k)', bygroups(Keyword,Text,Number)),
    (r'(\~>)(\s*)([0-9]+|k)', bygroups(Keyword,Text,Number)),
    (r'(rate)(\s*)(\([0-9]+,[0-9]+\))(\s*)(on)(\s*)('+fullid+')(\s*)(\()('+fullid+')(\))', bygroups(Keyword,Text,Number,Text,Keyword,Text,Name.Attribute,Text,Text,Name.Variable.Instance,Text)),
    (r'(rate)(\s*)(\([0-9]+,[0-9]+\))', bygroups(Keyword,Text,Number)),
    (r'(rate)(\s*)(\(n,p\))', bygroups(Keyword,Text,Number)),
    (r'', Text, '#pop'),
  ],

  'exprapply': [
    (r'\s+', Text),
    (r',', Text, 'expr'),
    (r'\)', Text, '#pop')
  ],

  'mergevar': [
    (r'\s+', Text),
    (r'('+fullid+')(\s*)', bygroups(Text, Text), '#pop'),
  ],

  'mergepairs': [
    (r'\s+', Text),
    (r'\)', Text, '#pop'),
    (r',', Text, 'mergepair'),
  ],

  'mergepair': [
    (r'\s+', Text),
    (r'('+fullid+r')(->)', bygroups(Name.Attribute,Keyword), ('#pop', 'expr')),
  ],

  'then': [
    (r'(\s*)(then)(\s*)', bygroups(Text,Keyword,Text), '#pop'),
  ],

  'else': [
    (r'(\s*)(else)(\s*)', bygroups(Text,Keyword,Text), '#pop'),
  ],
})

eq = merge(utils, expr, {
  'start-eqlhs': [
    (r'\s+', Text),
    (fullid, Name.Variable.Instance, ('#pop', 'mid-eqlhs'))
  ],

  'mid-eqlhs': [
    (r'\s+', Text),
    (r',', Text),
    (fullid, Name.Variable.Instance),
    (r'=', Text, ('#pop', 'scol', 'expr')),
  ],
})

defs = merge(utils, expr, eq, {
  'automaton-or-eq-def': [
    (r'\.\.\.', Text, ('#pop')),
    (r'automaton', Keyword, ('#pop', 'automaton')),

    (fullid, Name.Variable.Instance, ('#pop', 'mid-eqlhs')),
  ],

  'automaton': [
    (r'\s+', Text),
    (r'(\|)', Text, 'autostate'),
    (r'end', Keyword, '#pop')
  ],

  'autostate': [
    (r'\s+', Text),
    (fullid, Name.Attribute, ('#pop','autoarrow')),
  ],

  'autoarrow': [
    (r'\s+', Text),
    (r'->', Text, ('#pop', 'autost')),
  ],

  'autost': [
    (r'\s+', Text),
    (r'unless', Keyword, ('autostpostexpr', 'expr')),
    (r'var', Keyword, ('#pop', 'autodefs', 'local-list')),
    (r'', Text, ('#pop', 'autodefs')),
  ],

  'autostpostexpr': [
    (r'\s+', Text),
    (r'(then|continue)(\s+)('+fullid+r')(\s*)(;)', bygroups(Keyword, Text, Name.Attribute, Text, Text), ('#pop')),
  ],

  'autodefs': [
    (r'\s+',Text),
    (r'until', Keyword, ('#pop', 'autowt', 'autowtpostexpr', 'expr')),
    (r'\|', Text, ('#pop', 'autostate')),
    (r'end', Keyword, ('#pop', '#pop')),
    ('automaton', Keyword, 'automaton'),
    (fullid, Name.Variable.Instance, 'mid-eqlhs'),
  ],

  'autowt': [
    (r'\s+', Text),
    (r'until', Keyword, ('autowtpostexpr', 'expr')),
    (r'\|', Text, ('#pop', 'autostate')),
    (r'end', Keyword, ('#pop', '#pop')),
  ],

  'autowtpostexpr': [
    (r'\s+', Text),
    (r'(then)(\s+)('+fullid+r')(\s*;)', bygroups(Keyword,Text,Name.Attribute,Text), ('#pop')),
  ],
})

class PreludeLexer(RegexLexer):
  tokens = merge(utils, imported_node, sensact, expr, vardecl, defs, {
    'root': [
      (r'\s+', Text),
      (r'--.*', Comment.Singleline),
      (r'node', Keyword, 'node'),
      (r'imported', Keyword, 'imported-node'),
      (r'sensor', Keyword, 'sensact-mid'),
      (r'actuator', Keyword, 'sensact-mid'),
    ],

    'node': [
      (r'\s+', Text),
      (fullid, Text, ('#pop','nodedefs','nodelocals','nodevardecls','nodepostinputs','nodevardecls')),
    ],

    'nodevardecls': [
      (r'\s+', Text),
      (r'\(', Text, ('#pop','vardecl')),
    ],

    'nodepostinputs': [
      (r'\s+', Text),
      (r'returns', Keyword, '#pop'),
    ],

    'nodelocals': [
      (r'\s+', Text),
      (r'var', Keyword, ('#pop','local-list')),
      (r'', Text, ('#pop')),
    ],

    'nodedefs': [
      (r'\s+', Text),
      (r'let', Keyword, ('#pop','nodedef')),
    ],

    'nodedef': [
      (r'\s+', Text),
      (r'tel', Keyword, '#pop'),
      (r'', Text, 'automaton-or-eq-def')
    ],
  })

class PreludeDefLexer(RegexLexer):
  tokens = merge(utils, expr, defs, {
    'root': [
      (r'', Text, 'automaton-or-eq-def')
    ]
  })

class PreludeExprLexer(RegexLexer):
  tokens = merge(utils, expr, {
    'root': [
      (r'', Text, 'expr'),
    ],
  })

class PreludeKeywordLexer(RegexLexer):
  tokens = merge(utils, {
    'root': [
      (r'\s+', Text),
      (r'.+', Keyword)
    ]
  })

class PreludeClockLexer(RegexLexer):
  tokens = merge(utils, {
    'root': [
      (r'\s+', Text),
      (r'(rate)(\s*)(\([0-9a-zA-Z]+,\s*[0-9a-zA-Z]+\))(\s*)', bygroups(Keyword, Text, Number, Text), 'opt-act-cond'),
      (r'(on)(\s*)('+fullid+r')(\s*)(\()(\s*)('+fullid+r')(\s*)(\))',
       bygroups(Keyword, Text, Name.Constant, Text, Punctuation, Text, Name, Text, Punctuation))
    ],

    'opt-act-cond': [
      (r'\s+', Text),
      (r'(on)(\s*)('+fullid+r')(\s*)(\()(\s*)('+fullid+r')(\s*)(\))',
       bygroups(Keyword, Text, Name.Constant, Text, Punctuation, Text, Name, Text, Punctuation))
    ],
  })

class PreludeEqLexer(RegexLexer):
  tokens = merge(utils, eq, {
    'root' : [
      ('', Text, 'start-eqlhs'),
    ]
  })

class PreludeVDeclLexer(RegexLexer):
  tokens = merge(vardecl, {
    'root' : [
      ('', Text, 'vardecl')
    ]
  })

class PreludeCkDeclLexer(RegexLexer):
  tokens = merge(vardecl, {
    'root' : [
      ('', Text, 'vardecl-opt-type-opt-clock')
    ]
  })

class PreludeFnLexer(RegexLexer):
  tokens = {
    'root': [
      ('.', Name.Function)
    ]
  }

class PreludeROpLexer(RegexLexer):
  tokens = merge(expr, {
    'root': [
      ('', Text, ('#pop', 'expr_ROp'))
    ]
  })

class RawLexer(RegexLexer):
  tokens = { 'root' : [ ('.+', Text) ] }

class AttributeLexer(RegexLexer):
  tokens = { 'root' : [ ('.+', Name.Attribute) ] }

class NewClockLexer(RegexLexer):
  tokens = {
    'root' : [
      (r'(rate)(\s*)(\([0-9a-zA-Z]+-[0-9a-zA-Z]+,\s*[0-9a-zA-Z]+\))(\s*)', bygroups(Keyword, Text, Number, Text)),
    ]
  }

if __name__ == '__main__':
  x = PreludeDefLexer()

  x = PreludeLexer()

  x = PreludeExprLexer()
  s = "e ~> k"
  for x in x.get_tokens(s):
    print(x)

  x = PreludeExprLexer()
  s = "x"
  for x in x.get_tokens(s):
    print(x)

  x = PreludeExprLexer()
  s = "e rate (10,0)"
  for x in x.get_tokens(s):
    print(x)

  x = PreludeKeywordLexer()
  s = "*^k"
  for x in x.get_tokens(s):
    print(x)

  x = PreludeClockLexer()
  s = "rate (10,0) on true(c)"
  for x in x.get_tokens(s):
    print(x)


  x = PreludeEqLexer()
  s = "x' = x when true(c)"
  for x in x.get_tokens(s):
    print(x)

  x = PreludeCkDeclLexer()

  x = PreludeROpLexer()
  s = "/^2"
  for x in x.get_tokens(s):
    print(x)
