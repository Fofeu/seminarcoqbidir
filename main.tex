\documentclass{beamer}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage{lmodern}

\usepackage[dashed=false,backend=biber,style=authoryear]{biblatex}
\addbibresource{ffort-bib/biblio.bib}
\addbibresource{ffort-bib/images.bib}
\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map{
      \step[fieldsource=author,
      match=Fort,
      final]
      \step[fieldset=keywords, fieldvalue=own]
    }
  }
}

\newcommand{\ptmed}{\textperiodcentered}

\usepackage{import}
\import{ffort-latex/}{ffort-commands.sty}
\import{ffort-latex/}{ffort-beamer.sty}

\usepackage{subfiles}

\usepackage{tabularx}
\newcolumntype{C}{>{\centering\arraybackslash}X}

\usepackage{svg}
\usepackage{csquotes}
\usepackage{mathpartir}
\usetikzlibrary{tikzmark}
\usepackage{hf-tikz}

\title{WiP: Provably Sound Type Systems in Coq}

\author{Frédéric Fort}

\institute{}

\date[07/11/2023]{7 November 2023}

\newcommand{\tocname}{Table of Contents}

\AtBeginSection[] {
  \begin{frame}<beamer>{\tocname}
    \tableofcontents[
    currentsection,
    sectionstyle=show/shaded,
    subsectionstyle=show/show/hide]
  \end{frame}
}

\AtBeginSubsection[] {
  \begin{frame}<beamer>{\tocname}
    \tableofcontents[
    sectionstyle=show/hide,
    subsectionstyle=show/shaded/hide]
  \end{frame}
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Content warning}

  \begin{itemize}
  \item Heavily Work-in-Progress
  \item May not me innovative at all
  \item The demo doesn't work on this machine
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{About myself}

  \begin{itemize}
  \item PhD in PL for real-time systems
  \item[$\Rightarrow$] Synchronous semantics of multi-rate multi-mode systems
  \item Type system-first view of programming languages
  \item Written type systems in way too many languages
  \end{itemize}

\end{frame}

\section{Motivation}

\begin{frame}
  \frametitle{Type System Formalization vs Implementation}

  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=\linewidth]{img/SystemF.png}
      \end{center}
    \end{column}

    \begin{column}{0.5\textwidth}
      \begin{center}
        \includegraphics[width=\linewidth]{img/OcamlType.png}
      \end{center}
    \end{column}
  \end{columns}

  \quad\\\quad

  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Declarative style
      \item Requires an oracle
      \item[$\Rightarrow$] Bad match for actual implementations
      \end{itemize}
    \end{column}

    \begin{column}{0.5\textwidth}
      \begin{itemize}
      \item Algorithmic by essence
      \item Oracles don't exist
      \item[$\Rightarrow$] Not a spec compliant implementation
      \end{itemize}
    \end{column}
  \end{columns}

\end{frame}

\begin{frame}
  \frametitle{Trust-issues}

  \begin{enumerate}
    \only<1>{\item ``Source: Trust me.''}
    \only<2->{\item Handwritten proof}
    \only<3->{\item Machine-checked (mechanized) proof}
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Goal}

  \begin{block}{Define a type system inside a proof-assistant}
    \begin{itemize}
    \item Typer checker respects the specification
    \item Prove meaningful properties about it
    \end{itemize}
    \end{block}
\end{frame}

\section{Scientific Context}

\begin{frame}
  \frametitle{Choice of type system}

  \begin{center}
    \fullcite{dunfield2013}
  \end{center}

  \begin{itemize}
  \item<1-> \emph{Complete}: $\forall \Psi\ e\ A, \Psi \vdash e : A \lor \neg \Psi \vdash e : A$
  \item<2-> \emph{Easy}: Claim
  \item<3-> \emph{Bidirectional typechecking}:
    \begin{itemize}
    \item Synthesis: $\Psi \vdash e \Rightarrow A$
    \item Checking: $\Psi \vdash e \Leftarrow A$
    \end{itemize}
  \item<4-> \emph{Higher-rank polymorphism}:
    \begin{itemize}
    \item $\forall \alpha.\ \alpha \rightarrow \alpha$
    \item $\forall \alpha. (\forall \beta. \beta \rightarrow \beta) \rightarrow \alpha \rightarrow \alpha$
    \item $\forall \alpha. ((\forall \beta. \beta \rightarrow \beta) \rightarrow \alpha) \rightarrow \alpha$
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Two-step definition}

  \begin{enumerate}
  \item<1-> Declarative system (specification) verified wrt. System F
    \begin{itemize}
    \item Completeness (modulo annotation) of synthesis
    \item Soundness (modulo annotation) of synthesis and checking
    \end{itemize}
  \item<2-> Algorithmic system verified wrt. the declarative system
    \begin{itemize}
    \item Decidability
    \item Soundness
    \item Completeness
    \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Declarative System}

  \begin{center}
    \adjustbox{max width=\linewidth}{\includegraphics{img/DeclarativeSystem.png}}
  \end{center}

  \begin{center}
    \adjustbox{max width=0.5\linewidth}{\includegraphics{img/DeclarativeSubtyping.png}}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Algorithmic System}

  \begin{center}
    \includegraphics[width=\linewidth]{img/AlgorithmicSystem.png}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Algorithmic Subtyping}

  \begin{center}
    \includegraphics[scale=0.25]{img/AlgorithmicSubtyping.png}
  \end{center}
\end{frame}

\section{Process}

\begin{frame}
  \frametitle{General recommendations}

  \begin{itemize}
  \item<1-> Increase the size of your language step by step
  \item<2-> Create a (well-sorted) collection of lemmas
  \item<3-> Accept that the paper takes shortcuts sometimes
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Language specification}

  \begin{minted}{coq}
Inductive type :=
| TUnit
| TFun (tin tout : type)
| TExVar (v : nat)
.

Inductive expr :=
| Unt
| Var (n : nat)
| Lam (v : nat) (e : expr)
| App (f : expr) (arg : expr)
| Ant (e : expr) (annot : type)
.
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Declarative type system 1/2}
  \begin{minted}[fontsize=\small]{coq}
Inductive typeSub : type -> type -> Prop :=
| DeSubUnit : typeSub TUnit TUnit
| DeSubTFun : forall ti1 to1 ti2 to2, typeSub ti2 ti1 ->
  typeSub to1 to2 -> typeSub (TFun ti1 to1) (TFun ti2 to2).

Inductive typeChk : context -> expr -> type -> Prop :=
| DeChkSub: forall ctx e t, DeCtxWF ctx ->
  (exists tsyn, typeSyn ctx e tsyn /\ typeSub tsyn t) ->
  typeChk ctx e t
| DeChkUnt : forall ctx, DeCtxWF ctx -> typeChk ctx Unt TUnit
| DeChkLam : forall ctx v e ti to, DeCtxWF ctx ->
  typeChk ((BVar v ti)::ctx) e to ->
   typeChk ctx (Lam v e) (TFun ti to)
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Declarative type system 2/2}
  \begin{minted}[fontsize=\small]{coq}
with typeSyn : context -> expr -> type -> Prop :=
| DeSynVar : forall ctx v t, DeCtxWF ctx ->
  CtxIn ctx (BVar v t) -> typeSyn ctx (Var v) t
| DeSynAnt : forall ctx e t, DeCtxWF ctx -> DeTypeWF ctx t ->
  typeChk ctx e t -> typeSyn ctx (Ant e t) t
| DeSynUnt : forall ctx, DeCtxWF ctx -> typeSyn ctx Unt TUnit
| DeSynLam : forall ctx v e ti to, DeCtxWF ctx ->
  DeTypeWF ctx (TFun ti to) -> typeChk ((BVar v ti)::ctx) e to ->
  typeSyn ctx (Lam v e) (TFun ti to)
| DeSynApp : forall ctx f arg t, DeCtxWF ctx ->
  (exists tf, typeSyn ctx f tf /\ typeApp ctx tf arg t) ->
  typeSyn ctx (App f arg) t

with typeApp : context -> type -> expr -> type -> Prop :=
| DeAppFun : forall ctx ti to e, DeCtxWF ctx -> typeChk ctx e ti ->
  typeApp ctx (TFun ti to) e to
.
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Declarative Theorems}
  \begin{minted}[fontsize=\small]{coq}
Theorem Completeness:
  forall ctx e t, DeCtxWF ctx -> typeInf ctx e t ->
  exists e', typeSyn ctx e' t /\ erasure e' e.
...

Theorem SoundnessCheking:
  forall ctx e t, DeCtxWF ctx -> typeChk ctx e t ->
  exists e', typeInf ctx e' t /\ erasure e e'.
...

Theorem SoundnessSynthesis:
  forall ctx e t, DeCtxWF ctx -> typeSyn ctx e t ->
  exists e', typeInf ctx e' t /\ erasure e e'.
...
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Algorithmic type system 1/4}
  \begin{minted}[fontsize=\small]{coq}
Inductive typeSub : context -> type -> type -> context -> Prop :=
| AlSubTUnit  : forall ctx, AlCtxWF ctx ->
  typeSub ctx TUnit TUnit ctx
| AlSubTExVar : forall ctx v, AlCtxWF ctx -> CtxMem ctx KExV v ->
  typeSub ctx (TExVar v) (TExVar v) ctx
| AlSubTFun   : forall ictx ti1 to1 ti2 to2 octx, AlCtxWF ictx ->
  (exists ctx' to1' to2', typeSub ictx ti2 ti1 ctx' /\
  ctxAppl ctx' to1 to1' /\ ctxAppl ctx' to2 to2' /\
  typeSub ctx' to1' to2' octx) ->
  typeSub ictx (TFun ti1 ti2) (TFun to1 to2) octx
| AlSubInstL  : forall ictx v t octx, AlCtxWF ictx ->
  CtxMem ictx KExV v -> non_free_typevar v t ->
  typeInL ictx v t octx -> typeSub ictx (TExVar v) t octx
| AlSubInstR  : forall ictx t v octx, AlCtxWF ictx ->
  CtxMem ictx KExV v -> non_free_typevar v t ->
  typeInR ictx t v octx -> typeSub ictx t (TExVar v) octx
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Algorithmic type system 2/3}
  \begin{minted}[fontsize=\small]{coq}
Inductive typeChk : context -> expr -> type -> context -> Prop :=
| AlChkSub : forall ictx e t octx,
    AlCtxWF ictx ->
    (exists tsyn octx', (typeSyn ictx e tsyn octx' /\
     exists tsyn', ctxAppl octx' tsyn tsyn' /\
     exists t', ctxAppl octx' t t' /\
     typeSub octx' tsyn' t' octx
    ))
    -> typeChk ictx e t octx
| AlChkUnt : forall ctx, AlCtxWF ctx -> typeChk ctx Unt TUnit ctx
| AlChkLam : forall ictx v e ti to octx,
  (exists octx', typeChk (BVar v ti::ictx) e (TFun ti to) octx'
  /\ exists ctx_post, ctxSplit octx' (BVar v ti) octx ctx_post)
    -> typeChk ictx (Lam v e) (TFun ti to) octx
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Algorithmic type system 3/3}
  \begin{minted}[fontsize=\footnotesize]{coq}
with typeSyn : context -> expr -> type -> context -> Prop :=
| AlSynUnt : forall ctx, AlCtxWF ctx -> typeSyn ctx Unt TUnit ctx
| AlSynVar : forall ctx n t, AlCtxWF ctx -> CtxIn ctx (BVar n t)
  -> typeSyn ctx (Var n) t ctx
| AlSynLam : forall ictx v e ti to octx,
  AlCtxWF ictx -> (exists octx' ctx_post,
  typeChk (BVar v (TExVar ti)::(BExV to)::(BExV ti)::ictx)
    e (TExVar to) octx'
  /\ ctxSplit octx' (BVar v (TExVar ti)) octx ctx_post)
  -> typeSyn ictx (Lam v e) (TFun (TExVar ti) (TExVar to)) octx
| AlSynApp : forall ictx f arg t octx, (exists tf octx',
  typeSyn ictx f tf octx' /\ exists tf', ctxAppl octx' tf tf' /\
  typeApp octx' tf' arg t octx) -> typeSyn ictx (App f arg) t octx
| AlSynAnt : forall ctx ctx' e t, AlCtxWF ctx -> AlTypeWF ctx t ->
  typeChk ctx e t ctx' -> typeSyn ctx (Ant e t) t ctx'

with typeApp : context -> type -> expr -> type -> context -> Prop :=
| AlAppFun : forall ictx ti to e octx, AlCtxWF ictx ->
  typeChk ictx e ti octx -> typeApp ictx (TFun ti to) e to octx
.
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Algorithmic Theorems (Decidability)}
  \begin{minted}[fontsize=\small]{coq}
Definition Decidable (A: Prop) := A \/ ~ A.

Theorem DecidabilitySubtyping:
  forall ictx t1 t2, AlCtxWF ictx ->
  typeWF ictx t1 -> typeWF ictx t2 ->
  Decidable (exists t1' t2' octx,
    ctxAppl ictx t1 t1' /\ t1 = t1' /\
    ctxAppl ictx t2 t2' /\ t2 = t2' /\
    typeSub ictx t1 t2 octx).

Theorem DecidabilitySynthesis:
  forall ictx e, AlCtxWF ictx ->
    Decidable (exists t octx, typeSyn ictx e t octx).

Theorem DecidabilityChecking:
  forall ictx e t, AlCtxWF ictx -> typeWF ictx t ->
    Decidable ( exists octx, typeChk ictx e t octx ).
  \end{minted}
\end{frame}



\begin{frame}
  \frametitle{Demo}

  \begin{block}{Doesn't work on this machine}
    Find the code at \url{https://gitlab.com/Fofeu/coqbidir}
  \end{block}

\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
